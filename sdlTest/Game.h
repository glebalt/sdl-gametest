//
// Created by glebo on 30.04.2024.
//

#ifndef SDLTEST_GAME_H
#define SDLTEST_GAME_H
#endif //SDLTEST_GAME_H
#include "include/SDL.h"
#include "include/SDL_image.h"
#include "Gameobject.h"

class Game{
public :
    static float timer;
  static  SDL_Renderer* gameRendererMain;

    Game();
    ~Game();
    void init(const char* title,int xpos,int ypos,int width,int height,bool fullscreen);

    void Update();
    void Render();
    void clean();
    void handleEvents();

    bool running(){return  isRunning;}

private:
    int count = 0;
   bool isRunning;
    SDL_Window* window;

    SDL_Rect* rect;

};