
#include "Game.h"
#pragma once

SDL_Texture* TextureManager::LoadTexture(const char *fileName) {
    SDL_Surface* surface = IMG_Load(fileName);
    SDL_Texture* tex = SDL_CreateTextureFromSurface(Game::gameRendererMain,surface);
    SDL_FreeSurface(surface);
    return tex;


}

void TextureManager::Draw(SDL_Texture* texture,SDL_Rect dstRect){
    SDL_RenderCopy(Game::gameRendererMain,texture,NULL, &dstRect);
}