//
// Created by glebo on 15.05.2024.
//

#include "include/SDL.h."
#pragma once

class Tilemap{

public:

   Tilemap();
   ~Tilemap();

   void LoadMap(int arrr[20][25]);
   void DrawMap();
private :
SDL_Rect srcRect,destRect;
SDL_Texture* grass;
SDL_Texture* dirt;
SDL_Texture* water;

int map[20][25];
};
