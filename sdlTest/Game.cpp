//
// Created by glebo on 30.04.2024.
#ifndef SDLTEST_GAME_H
#define SDLTEST_GAME_H
#endif //SDLTEST_GAME_H
#include "Game.h"
#include "iostream"
#include "Tilemap.h"
Game::Game() {}
Game::~Game() {}

Gameobject* player;
Gameobject *enemy;
Gameobject* textureTest;
Tilemap* tilemap;


SDL_Renderer* Game::gameRendererMain = nullptr;
//Start method
void Game::init(const char *title, int xpos, int ypos, int width, int height, bool fullscreen) {
    if(SDL_Init(SDL_INIT_EVERYTHING) == 0)
    {
        int flags = SDL_WINDOW_RESIZABLE;
        if(fullscreen){
            flags |=  SDL_WINDOW_FULLSCREEN;
        }

        std::cout << "Subsystems initialized" << std::endl;
        window = SDL_CreateWindow(title,xpos,ypos,width,height,flags);
        if(window){
            std::cout << "window created" << std::endl;
        }

       Game::gameRendererMain = SDL_CreateRenderer(window,-1,0);
        if( Game::gameRendererMain){
            SDL_SetRenderDrawColor( Game::gameRendererMain,0,255,0,255);
        }


  player =  new Gameobject(("C:/Users/glebo/CLionProjects/sdlTest/assets/text2.jpg"),10,10);
    enemy = new Gameobject("C:/Users/glebo/CLionProjects/sdlTest/assets/test.jpg",1000,20);
    textureTest = new Gameobject("C:/Users/glebo/CLionProjects/sdlTest/assets/grass.png",600,40);




 tilemap = new Tilemap();






        isRunning = true;


    }
    else{
        isRunning = false;
    }
}
float Game::timer = 0;
void Game::Update() {
timer = SDL_GetTicks();
    enemy->Update();
    player->Update();
    textureTest->Update();
}


void Game::Render() {

    SDL_RenderClear( Game::gameRendererMain);
    tilemap->DrawMap();
    SDL_RenderPresent( Game::gameRendererMain);


}
void Game::handleEvents() {
    SDL_Event event;
    SDL_PollEvent(&event);
    switch(event.type){
        case SDL_QUIT:
            isRunning = false;
            break;
        default:break;
        }
  //  if(timer > 2000){
   //     isRunning = false;
   // }



}
void Game::clean() {
    SDL_DestroyWindow(window);
    SDL_DestroyRenderer( Game::gameRendererMain);
}
