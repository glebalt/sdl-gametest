//
// Created by glebo on 11.05.2024.
//


#include "include/SDL.h"
#include "TextureManager.h"

#include "iostream"
#pragma once
class Gameobject{
public:
    SDL_Rect srcRect;
  SDL_Rect dstrRect ;
    Gameobject(const char* fileName,int xpos,int ypos){
        objTexture = TextureManager::LoadTexture(fileName);
        if(objTexture){
        dstrRect.x = xpos;
        dstrRect.y = ypos;
        }
    }

    void Update();



    void Render(SDL_Rect* rect);



private:

    SDL_Texture* objTexture;
    int xPos;
    int yPos;


};